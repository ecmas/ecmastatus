ecmastatus
============

=== Personal Usage Warning ===

This is a simple webserver to announce not so important updates on my blog

---

## Setup
Clone this repo to your desktop and run `go build`

Setup your 'admin' password as `ecmakey` environment variable(1)

(1) - Setting your password as an environment variable is not really secure, you can tweak the code and try something else for yourself
---

## Usage
Once its built, you can run  `./EcmaStatus` to start the server. You will then be able to access it at localhost:9000

To change status head to localhost:9000/add 

---

## License
>You can check out the full license [here](https://gitlab.com/leJad/ecmastatus/-/blob/master/LICENSE.md)

This project is licensed under the terms of the **WTFPL** license.
