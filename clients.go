package main

import (
	"net/http"
	"github.com/gorilla/websocket"
	"log"
)

var Clients = make(map[*Client]bool)

type Client struct {
    // The actual websocket connection.
    conn *websocket.Conn
}

// Return a new client
func newClient(conn *websocket.Conn) *Client {
    return &Client{
        conn: conn,
    }
}

// Broadcast messages to every client
func broadcast(msgChan chan string){
	for msg := range msgChan {
		for client, _ := range Clients{

			err := client.conn.WriteMessage(websocket.TextMessage, []byte(msg))
			if err != nil {
				log.Println(err)

				delete(Clients, client)
			}
		}
		addStatus(msg)
	}
}


func serveWS(w http.ResponseWriter, r *http.Request) {
	if !websocket.IsWebSocketUpgrade(r){
		w.Write([]byte("Try using websocket instead :-)"))
		return
	}

	msgChan := make(chan string)
	clientChan := make(chan *Client, 1)

	ws, err := wsUpgrader.Upgrade(w, r, nil)
	checkForErrors(err)
	client := newClient(ws)

	clientChan <- client
	Clients[client] = true
	
	log.Println("Client Connected")

	go readWS(ws, msgChan, clientChan)
	go broadcast(msgChan)
}