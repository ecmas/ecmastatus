package main

import(
  "net/http"
  "html/template"
  "log"
  "os"
  "regexp"
  "golang.org/x/crypto/bcrypt"
  "github.com/microcosm-cc/bluemonday"
  "github.com/gorilla/websocket"
)

type page struct {
  Data template.HTML
}

var LatestStatus string

var wsUpgrader = websocket.Upgrader{
  WriteBufferSize:	1024, 
  ReadBufferSize:	1024,
  CheckOrigin:	func(r *http.Request) bool { return true },
}

func checkForErrors(err error){
  if err != nil{
    log.Println(err)
    return
  }
}

func addStatus(replace string){
  // Function name is stupid, i know
  // Replace newlines with <br>
  re := regexp.MustCompile(`\r?\n`)
  LatestStatus = re.ReplaceAllString(replace, "<br>")
}

// Get the latest status and connect to websocket (from browser)
func clientPage(w http.ResponseWriter, r *http.Request){
  sanitizer := bluemonday.UGCPolicy()

  // Sanitize string
  LatestStatus = sanitizer.Sanitize(LatestStatus)

  // I need unescaped html to be able to use <br> tag
  p := page{template.HTML(LatestStatus)}
  htmlTemplate, err := template.ParseFiles("./public/index.html")
  checkForErrors(err)
  htmlTemplate.Execute(w, p)
}

// Think of it as admin panel. I use it to change current status
func opPage(w http.ResponseWriter, r *http.Request){
  t, _ := template.ParseFiles("./public/status.html")
  t.Execute(w, nil)
}


// Read, recieve, send messages and all that shit
func readWS(conn *websocket.Conn, msg chan string, clientChan chan *Client){
  // Recieve new client
  client := <- clientChan

  // Read first message to compare with password
  _, p, _ := client.conn.ReadMessage();

  // First compare our hashed password with input, if true;
  // Listen for messages and if we get any message, broadcast it (check clients.go)
  // if false;
  // don't broadcast it
 
  hashedPass := []byte(os.Getenv("ecmakey")) // Get hashed password from env vars (this is not really secure, change it if you want to)
  
  if bcrypt.CompareHashAndPassword(hashedPass, p) == nil{
    for{
      _, p, err := client.conn.ReadMessage()
      if err != nil {
      	log.Println(err)
      	client.conn.Close()
        delete(Clients, client)
      	break
      }

      msg <- string(p)
    }
  }

  // Listen to see if client has disconnected
  for{
    _, _, err := client.conn.ReadMessage()
    if err != nil {
      log.Println(err)
      client.conn.Close()
      delete(Clients, client)
      break
    }
  }

}


func main(){
  http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
    serveWS(w, r)
  })

  http.HandleFunc("/add", opPage)
  http.HandleFunc("/", clientPage)
  
  log.Println("Listening on :9000")
  log.Fatal(http.ListenAndServe(":9000", nil))
}
