module gitlab.com/leJad/EcmaStatus

go 1.18

require (
	github.com/gorilla/websocket v1.5.0
	github.com/microcosm-cc/bluemonday v1.0.18
	golang.org/x/crypto v0.0.0-20220321153916-2c7772ba3064
)

require (
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/gorilla/css v1.0.0 // indirect
	golang.org/x/net v0.0.0-20211112202133-69e39bad7dc2 // indirect
)
